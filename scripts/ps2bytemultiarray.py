#!/usr/bin/python

import roslib
roslib.load_manifest('diagnostic_updater')
import rospy
import diagnostic_updater
import diagnostic_msgs
import time
from std_msgs.msg import ByteMultiArray
from sensor_msgs.msg import Joy

pub = rospy.Publisher('ps3String', ByteMultiArray, queue_size = 20)
key_err = 0
key_warn = 0

def l2_diagnostic(stat):
	if key_err:
		stat.summary(diagnostic_msgs.msg.DiagnosticStatus.ERROR, "Emergency Stop (L2)!")
	else:
		stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "No Emergency.")
	stat.add("Diagnostic Name", "Emergency Button (L2)")
	return stat

def r2_diagnostic(stat):
	if key_warn:
		stat.summary(diagnostic_msgs.msg.DiagnosticStatus.WARN, "Warning (R2)!")
	else:
		stat.summary(diagnostic_msgs.msg.DiagnosticStatus.OK, "Stable.")
	stat.add("Diagnostic Name", "Warning Button (R2)")
	return stat

def rosCallback(msg):

	global key_err
	global key_warn
	a = ByteMultiArray()

	if msg.buttons[0]:
		print "select tusuna basildi!"
		a.data.append(0)
	if msg.buttons[3]:
		print "start tusuna basildi!"
		a.data.append(3)
	if msg.buttons[4]:
		print "ust tusuna basildi!"
		a.data.append(4)
	if msg.buttons[5]:
		print "sag tusuna basildi!"
		a.data.append(5)
	if msg.buttons[6]:
		print "alt tusuna basildi!"
		a.data.append(6)
	if msg.buttons[7]:
		print "sol tusuna basildi!"
		a.data.append(7)
	if msg.buttons[8]:
		print "L2 tusuna basildi!"
		a.data.append(8)
		key_err = 1
	else:
		key_err = 0
	if msg.buttons[9]:
		print "R2 tusuna basildi!"
		a.data.append(9)
		key_warn = 1
	else:
		key_warn = 0
	if msg.buttons[10]:
		print "L1 tusuna basildi!"
		a.data.append(10)
	if msg.buttons[11]:
		print "R1 tusuna basildi!"
		a.data.append(11)
	if msg.buttons[12]:
		print "ucgen tusuna basildi!"
		a.data.append(12)
	if msg.buttons[13]:
		print "yuvarlak tusuna basildi!"
		a.data.append(13)
	if msg.buttons[14]:
		print "carpi tusuna basildi!"
		a.data.append(14)
	if msg.buttons[15]:
		print "kare tusuna basildi!"
		a.data.append(15)

	pub.publish(a)
	updater.update()

	

rospy.init_node('ps2bytearray')

updater = diagnostic_updater.Updater()
updater.setHardwareID("PS3 DualShock")
updater.add("Emergency Diagnostic", l2_diagnostic)
updater.add("Warning Diagnostic", r2_diagnostic)

updater.force_update()

sub = rospy.Subscriber('joy', Joy, rosCallback)
rospy.spin()

